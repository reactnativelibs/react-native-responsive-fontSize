import { isIphoneX } from "react-native-iphone-x-helper";
import { Platform, StatusBar, Dimensions } from "react-native";

const { width } = Dimensions.get("window");
// const standardLength = width > height ? height: width;
// const offset =
//   width > height ? 0 : Platform.OS === "ios" ? 78 : StatusBar.currentHeight; // iPhone X style SafeAreaView size in portrait

// const deviceWidth =
//   isIphoneX() || Platform.OS === "android"
//     ? standardLength - offset
//     : standardLength;

// export function RFPercentage(percent) {
//   const heightPercent = (percent * deviceWidth) / 100;
//   return Math.round(heightPercent);
// }
const scale = (size, standardScreenWidth) => width / standardScreenWidth * size;

// guideline height for standard 5" device screen is 680
export function RFValue(fontSize, standardScreenWidth = 375, factor = 0.5) {
  return Math.round(fontSize + (scale(fontSize, standardScreenWidth) - fontSize) * factor)
}
